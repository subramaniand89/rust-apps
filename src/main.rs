#[macro_use] extern crate rocket;
mod queuing;

#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

#[launch]
fn rocket() -> _ {
    //queuing::main("Hello");   //Testing queuing call!

    rocket::build().mount("/", routes![index,enqueue])
}

#[get("/api/queue/<input>")]
fn enqueue(input: &str) -> &'static str {
    queuing::main(input);
    "Added to queue"
}