use queue::Queue;

pub fn main(string:&str)
{
	let mut q = Queue::new();
	q.queue(string).unwrap();
	//q.queue("hello").unwrap();
	//q.queue("out").unwrap();

	while let Some(item) = q.dequeue() {
		println!("{}", item);
	}
}
