package learn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Caller {

	//calling "http://localhost:8080/hello" running in RUST
	public static void main(String[] args) {
		String input="";
		if (args.length>0)
		{
			input = args[0];
		}
	/*
		try {
			URL url = new URL("http://localhost:8000/hello");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/text");
	
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
	
			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
			conn.disconnect();

	  } catch (MalformedURLException e) {
		e.printStackTrace();
	  } catch (IOException e) {
		e.printStackTrace();
	  }
	  */

	  //calling with enQueue
	  input = input.equals("")?"Subbu":input;
	  System.out.println("input : "+ input);

	  String res = enQueue(input);
	  System.out.println(res);
	}

	public static String enQueue(String input){
		try {
				URL url = new URL("http://localhost:8000/api/queue/" + input);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Accept", "application/text");
	
				if (conn.getResponseCode() != 200) {
					throw new RuntimeException("Failed : HTTP error code : "
							+ conn.getResponseCode());
				}
				BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));
	
				String output;
				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {
					return output;
				}
				conn.disconnect();

		  } 
		  catch (MalformedURLException e) {
			e.printStackTrace();
		  }
		  catch (IOException e) {
			e.printStackTrace();
		  }
		  return "";
	}
}